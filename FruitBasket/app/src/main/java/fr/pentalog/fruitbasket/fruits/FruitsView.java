package fr.pentalog.fruitbasket.fruits;

/**
 * Created by Ionut Stirban on 27/02/2017.
 */

interface FruitsView {
    void showNumberOfFruits(int nrFruits);
}
