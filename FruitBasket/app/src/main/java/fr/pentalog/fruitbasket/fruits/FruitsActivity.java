package fr.pentalog.fruitbasket.fruits;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import fr.pentalog.fruitbasket.R;
import fr.pentalog.fruitbasket.fruits.domain.FruitsGateway;
import fr.pentalog.fruitbasket.fruits.domain.GetFruitsUsecase;

public class FruitsActivity extends AppCompatActivity implements FruitsView {
    private TextView txtBasked;
    private FruitsPresenter fruitsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruits);
        this.txtBasked = (TextView) findViewById(R.id.txt_basket);
        final FruitsGateway fruitsGateway = new InMemoryFruitsGateway();

        fruitsPresenter = new FruitsPresenter(this, new GetFruitsUsecase(fruitsGateway));
        setListeners();
    }

    private void setListeners() {
        findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fruitsPresenter.addOneFruit();
            }
        });
        findViewById(R.id.btn_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fruitsPresenter.removeOneFruit();
            }
        });
    }

    @Override
    public void showNumberOfFruits(int nrFruits) {
        this.txtBasked.setText(String.valueOf(nrFruits));
    }
}
