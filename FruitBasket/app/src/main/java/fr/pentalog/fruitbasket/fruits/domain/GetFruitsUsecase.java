package fr.pentalog.fruitbasket.fruits.domain;

import java.util.List;

/**
 * Created by Ionut Stirban on 24/02/2017.
 */

public class GetFruitsUsecase {
    private FruitsGateway fruitsGateway;

    public GetFruitsUsecase(FruitsGateway fruitsGateway) {
        this.fruitsGateway = fruitsGateway;
    }

    public List<Fruit> getFruits() {
        return fruitsGateway.getFruits();
    }
}
