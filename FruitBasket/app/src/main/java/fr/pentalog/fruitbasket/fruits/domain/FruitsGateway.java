package fr.pentalog.fruitbasket.fruits.domain;

import java.util.List;

/**
 * Created by Ionut Stirban on 24/02/2017.
 */

public interface FruitsGateway {
    List<Fruit> getFruits();

    void addFruit(Fruit fruit);

    void removeFruit(Fruit fruit);
}
