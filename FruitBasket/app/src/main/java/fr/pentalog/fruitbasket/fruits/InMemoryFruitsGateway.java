package fr.pentalog.fruitbasket.fruits;

import java.util.ArrayList;
import java.util.List;

import fr.pentalog.fruitbasket.fruits.domain.Fruit;
import fr.pentalog.fruitbasket.fruits.domain.FruitsGateway;

/**
 * Created by Ionut Stirban on 24/02/2017.
 */

class InMemoryFruitsGateway implements FruitsGateway {
    private List<Fruit> fruits = new ArrayList<>();

    InMemoryFruitsGateway() {
        for (int i = 0; i < 20; i++) {
            fruits.add(new Fruit());
        }
    }

    @Override
    public List<Fruit> getFruits() {
        return fruits;
    }

    @Override
    public void addFruit(Fruit fruit) {
        fruits.add(fruit);
    }

    @Override
    public void removeFruit(Fruit fruit) {
        if (!fruits.contains(fruit)) {
            throw new IllegalArgumentException("The fruit that you want to remove is not in the basket");
        }
        fruits.remove(fruit);
    }
}
