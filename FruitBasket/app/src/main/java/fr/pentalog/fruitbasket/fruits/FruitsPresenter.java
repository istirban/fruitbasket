package fr.pentalog.fruitbasket.fruits;

import java.util.List;

import fr.pentalog.fruitbasket.fruits.domain.Fruit;
import fr.pentalog.fruitbasket.fruits.domain.GetFruitsUsecase;

/**
 * Created by Ionut Stirban on 27/02/2017.
 */

class FruitsPresenter {
    private GetFruitsUsecase getFruitsUsecase;
    private FruitsView fruitsView;

    FruitsPresenter(FruitsView fruitsView, GetFruitsUsecase getFruitsUsecase) {
        this.getFruitsUsecase = getFruitsUsecase;
        this.fruitsView = fruitsView;
        getFruits();
    }

    private void getFruits() {
        List<Fruit> fruits = getFruitsUsecase.getFruits();
        fruitsView.showNumberOfFruits(fruits.size());
    }

    void addOneFruit() {
        //execute add fruits usecase
    }

    void removeOneFruit() {
        //execute remove fruits usecase
    }
}
